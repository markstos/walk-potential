// Allow storing your environment variables in .env
import dotenv from 'dotenv';
import dateformat from 'dateformat';
import Promise from 'bluebird';
import path from 'path';
import fs from 'fs';

import area from '@turf/area';
import center from '@turf/center';
import boolWithin from '@turf/boolean-within';
import boolPointInPolygon from '@turf/boolean-point-in-polygon';
import calcBbox from '@turf/bbox';
import buffer from '@turf/buffer';
import combine from '@turf/combine';
import { multiPolygon } from '@turf/helpers';
import hexGrid from '@turf/hex-grid';
import { getCoords, getType } from '@turf/invariant';
import { union } from 'polyclip-ts';
import FetchRetry from 'fetch-retry';
import scale from 'scale-number-range';
import OverpassFrontend from 'overpass-frontend';
import { fileURLToPath } from 'url';
import { IsoChroneError } from '../lib/errors.mjs';

// Retry failed network requests up to 3x
const fetch = FetchRetry(global.fetch);

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// Look for .env in the directory above ./scripts
process.chdir(path.join(__dirname, '../'));
dotenv.config();

// you may specify an OSM file as url, e.g. 'test/data.osm.bz2'
const overpassFrontend = new OverpassFrontend('//overpass-api.de/api/interpreter');

// Length of one of the six hex sides
const HEX_SIDE = 0.05;// in km
// "diameter" of a hex
const HEX_WIDTH = 2 * HEX_SIDE;
const HEX_AREA = 6495; // square meters

// How many concurrent requests can your isochrone server handle?
const CONCURRENCY = Number(process.env.WALK_POTENTIAL_ISOCHRONE_CONCURRENCY) || 16;
const WALK_POTENTIAL_AMENITIES_FILE = process.env.WALK_POTENTIAL_AMENITIES_FILE || './amenities.json';
const amenityQueries = JSON.parse(fs.readFileSync(WALK_POTENTIAL_AMENITIES_FILE).toString());

const amenityLayers = [];
// The `combine` step is lossy in that it fills in the donut holes, but it's OK for this use.
async function getCityBoundaryMultiPolygon() {
  let fc; // feature collection
  if (process.env.WALK_POTENTIAL_BOUNDARY_URL) {
    console.warn(`using WALK_POTENTIAL_BOUNDARY_URL=${process.env.WALK_POTENTIAL_BOUNDARY_URL}`);
    const response = await fetch(process.env.WALK_POTENTIAL_BOUNDARY_URL);
    if (response.ok) {
      fc = await response.json();
    }
  } else {
    console.warn('WALK_POTENTIAL_BOUNDARY_URL not set. Reading Boundary from STDIN.');
    fc = JSON.parse(fs.readFileSync('/dev/stdin').toString());
  }
  const simplified = combine(fc);
  return simplified.features[0];
}

const cityMultiPolygon = await getCityBoundaryMultiPolygon();

// The amenities bounding box is slightly larger than the city boundary
// so that the Walk Potential just inside the edge of the boundary can consider
// locations to walk to outside the boundary
// .8 km is roughly the distance of a 10 minute walk
const amenitiesBufferKm = 0.8;
const amenitiesBbox = calcBbox(buffer(cityMultiPolygon, amenitiesBufferKm));
console.warn('Calculated amenity bounding box 🔳', amenitiesBbox);

/*
  @param {String} query - An Overpass Query, ex: "nwr[amenity=bank];nwr[amenity=atm"
  @param {Object} bbox - Bounding box given as { minlat, maxlat, minlon, maxlon }
  @returns {Promise<Array>} features -- array of GeoJSON featurings the query.

*/
async function bboxQueryToJson(query, bbox) {
  // Accumulate an array of features as they are returned.
  const features = [];
  const [minlon, minlat, maxlon, maxlat] = bbox;

  return new Promise((resolve, reject) => {
    overpassFrontend.BBoxQuery(
      // EX: 'nwr[amenity=bank];nwr[amenity=atm];',
      query,
      {
        minlat, maxlat, minlon, maxlon,
      },
      // This could be optimized by returning a minimal number of properties
      { properties: OverpassFrontend.ALL },
      (_err, result) => {
        features.push(result.GeoJSON());
      },
      (err) => {
        if (err) { reject(err); }
        resolve(features);
      },
    );
  });
}

// Expects WALK_POTENTIAL_ISOCHRONE_URL to contain {{lat}} and {{lon}}
function getIsochroneURL(lat, lon) {
  return process.env.WALK_POTENTIAL_ISOCHRONE_URL.replace(/\{\{([^}]+)}\}/g, (_match, key) => {
    if (key === 'lat') {
      return lat;
    }
    if (key === 'lon') {
      return lon;
    }
    throw new Error(`Invalid placeholder: ${key}`);
  });
}

// get a lon and lat, return GeoJSON Feature <MultiPolygon> representing isochrone for that point.
// @throws {IsoChroneError} if there's not coverage or point is not near a road.
async function getIsoChrone(lon, lat) {
  // Currently a 10 Minute walk isochrome is hardcoded.

  const response = await fetch(getIsochroneURL(lat, lon), {
    headers: {
      'User-Agent': 'Walk Potential https://gitlab.com/markstos/walk-potential',
      Accept: 'application/json',
    },
  });
  if (response.ok) {
    const fc = await response.json(); // featureCollection
    return fc.features[0];
  }

  throw new IsoChroneError(response);
}

/* Return a Hex Grid that covers the City of Bloomington Boundary and is masked to it.
 * Each cell is initialized with a walkPotential of zero.
 * This is used to create a heatmap.
 * "Hexagons are preferable when your analysis includes aspects of connectivity or movement paths."
 * Ref: https://pro.arcgis.com/en/pro-app/2.7/tool-reference/spatial-statistics/h-whyhexagons.htm
 * Each cell is the width of 1/3 of block, which are about 300 meters on the short side.
 * Because we providing the radius, a cellSide value of 50 meters is used, or 0.05 kilometers
 * Ref: http://turfjs.org/docs/#hexGrid
 */
function getHexGrid(mask, bbox) {
  const options = {
    units: 'kilometers',
    mask,
    properties: {
      walkPotential: 0,
    },
  };
  return hexGrid(bbox, HEX_SIDE, options);
}

/**
  For points and polygons smaller than a single hex, generate an isochrone from the center.
  For larger polygons, have walk potential just by walking to the edge of them-- like a large
  park or massive building. For those, find the centers of the hex cells they contain and generate
  the isochrone of all those points.
  @param {Feature} - GeoJSON Feature to generate an isochrone for
  @return {Feature<Polygon>} Isochrone as a GeoJSON Polygon Feature
  @return {undefined} if there's no coverage for the point or it's not near a road.
*/
async function isoChroneForOneFeature(feature) {
  const [lon, lat] = getCoords(center(feature));
  return Promise.resolve(getIsoChrone(lon, lat))
    .catch(IsoChroneError, (err) => {
      console.warn('\t❓ No isochrone found', err.message, [lon, lat]);
      return undefined;
    });
}

// given array of GeoJSON features return a multiPolygon feature
async function getAmenityLayer(features) {
  // This alternate code could be used to load pre-cached amaneity layers
  // let layerFile;
  // try {
  //   layerFile = fs.readFileSync(path.join(`./amenities`,fileName))
  // }
  // catch (err) {
  //   console.error(`Reading ${fileName} failed.`)
  //   console.error("Error was: ", err)
  // }
  // const layer = JSON.parse(layerFile)

  const maybeExpandedFeatures = features.flatMap((feature) => {
    if ((getType(feature) === 'Polygon') && (area(feature) > HEX_AREA)) {
      // Find the bbox for the polygon.
      const bbox = calcBbox(feature);
      // Generate hexes that fit in the polygon
      const grid = getHexGrid(feature, bbox);
      // Return the centers of those hexes
      if (grid.features.length) {
        // Keep the point if it's within one hex width of the edge
        // To calculate that, buffer one hex width smaller and check if the points are outside that.
        const smallerPoly = buffer(feature, -HEX_WIDTH); // Turf.js defaults to km
        return grid.features
          .map(center)
          .filter((centerPoint) => {
            // The negative buffer process can cause an undefined result here, so reality-check
            if (smallerPoly && getType(smallerPoly) === 'Polygon') {
              return boolPointInPolygon(getCoords(centerPoint), feature)
                && !boolPointInPolygon(getCoords(centerPoint), smallerPoly);
            }
            // Small polygons that failed negative buffering don't need filtering.
            return true;
          });
      // If no hexes fit in the polygon, fallback to using the center.
      }
      return center(feature);
    }
    return feature;
  });

  if (maybeExpandedFeatures.length > features.length) {
    const diff = maybeExpandedFeatures.length - features.length;
    console.warn(`\t${diff} additional points added due to large polygons`);
  }

  // Each file contains a GeoJSON FeatureCollection with one or more features.
  // If there's more than one Feature, we update the the properties for all.
  const multiPolygonFeatures = await Promise.map(
    maybeExpandedFeatures,
    isoChroneForOneFeature,
    { concurrency: CONCURRENCY },
  );

  let i = 0;
  const total = multiPolygonFeatures.length;

  // Filter and union all the isochrones on the layer together and return;
  return multiPolygon(multiPolygonFeatures
    .filter(Boolean)
    .reduce((acc, feature) => {
      // Progress report for large sets
      i += 1;
      if (i % 100 === 0) {
        console.warn(`\t🔄 merged ${i}/${total} amenity polygons`);
      }
      return union(getCoords(acc), getCoords(feature));
    }));
}

await Promise.each(Object.keys(amenityQueries), async (amenityName) => {
  const query = amenityQueries[amenityName];
  // NOTE: All logging goes to STDERR because output goes to STDOUT!
  console.warn(`${amenityName}: 🏙️  Querying Overpass to find features within bbox`);

  const features = await bboxQueryToJson(query, amenitiesBbox);

  if (features.length) {
    console.warn(`${amenityName}: 🗺️  Querying isochrone server for ${features.length} isochrones`);
    const layer = await getAmenityLayer(features);

    // Write the file out to isochrones directory with a parallel file name.
    // This serves two purposes. First, it work as a cache. This script could
    // be modified to used cached isochrones if they exist.
    // Second, it allows you to load and visualize this intermediate result to check the result.
    fs.writeFileSync(path.join('./isochrones', `${amenityName}.geojson`), JSON.stringify(layer));

    // For actual calculations, store the layers in memory.
    amenityLayers.push(layer);
  } else {
    console.warn(`${amenityName}: 0️⃣  No features found in this category within bbox!`);
  }
});

// Load or generate a hex grid.
let cityGrid;
if (process.env.WALK_POTENTIAL_GRID_FILE) {
  cityGrid = JSON.parse(fs.readFileSync(process.env.WALK_POTENTIAL_GRID_FILE).toString());
} else {
  console.warn('🚧 Building hex grid. Takes a while.');
  const cityBbox = calcBbox(cityMultiPolygon);
  cityGrid = getHexGrid(cityMultiPolygon, cityBbox);
}

const totalCells = cityGrid.features.length;
let curCell = 0;

// Count the amenity layers so we can scale the values later
const numAmenityLayers = Object.keys(amenityLayers).length;

cityGrid.features.forEach((cell) => {
  curCell += 1;
  if (curCell % 1000 === 0) {
    console.warn(`${curCell}/${totalCells} 👟 calculating walkPotential`);
  }
  const cellCenter = center(cell);

  // shorthand
  const props = cell.properties;

  // Important to reset values when updating a pre-existing grid.
  props.walkPotential = 0;

  // Score an additional point for this cell for each amenity layer that contains it.
  amenityLayers.forEach((layer) => {
    if (boolWithin(cellCenter, layer)) {
      props.walkPotential += 1;
    }
  });

  // scale walkPotential from 0-100
  props.walkPotential = scale(props.walkPotential, 0, numAmenityLayers, 0, 100);
});

// For reference, add the last updated date to the Feature Collection
// This is a non-standard GeoJSON field name and is being added for reference
// more than automated use.
cityGrid.walkPotentialUpdatedDate = (new Date()).toISOString();

// All done! Write final hex grid to STDOUT. Use the "walkPotential" property to color it on a map.
fs.writeFileSync('/dev/stdout', JSON.stringify(cityGrid));

console.warn('DONE. 🏁 walkPotential added or updated in grid.');

