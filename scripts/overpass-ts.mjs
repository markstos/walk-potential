/*
 Basic example of querying Overpass Turbo for an amenity with an area
 and print the result to STDOUT as GeoJSON
*/

// EX: 'nwr[amenity=bank];nwr[amenity=atm];',
import OverpassFrontend from 'overpass-frontend';

const QUERY = 'nwr[amenity=bank]';
// This bounding box matches the City of Bloomington Boundary. Update after Annexation.
const BOUNDING_BOX = {
  minlat: 39.1213, maxlat: 39.2214, minlon: -86.5919, maxlon: -86.4711,
};

const overpassFrontend = new OverpassFrontend('//overpass-api.de/api/interpreter');

async function bboxQueryToJson(query) {
  // Accumulate an array of features as they are returned.
  const features = [];
  return new Promise((resolve, reject) => {
    overpassFrontend.BBoxQuery(
      query,
      BOUNDING_BOX,
      // This could be optimized by returning a minimal number of properties
      { properties: OverpassFrontend.ALL },
      (err, result) => {
        features.push(result.GeoJSON());
      },
      (err) => {
        if (err) { reject(err); }
        resolve(features);
      },
    );
  });
}

const result = await bboxQueryToJson(QUERY);
console.log(JSON.stringify(result));
