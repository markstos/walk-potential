
Start to collect data and scripts for testing.

See code comments in scripts in the [./manual](./manual) directory to see what they do.

Some test data is in [./data](./data) directory including both some amenity data and also some
generated isochrones, which can be used to compare this generation of the algorithm with a future
generation.

The "GeomentryCollection" GeoJSON features there were constructed by hand from other GeoJSON files
to visualize the relationships on https://geojson.io/
