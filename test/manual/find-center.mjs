/*

Given GeoJSON on STDIN, print GeoJSON Point of center to STDOUT

*/
import fs from 'fs';
import center from '@turf/center';

const geojson = JSON.parse(fs.readFileSync('/dev/stdin').toString());
fs.writeFileSync('/dev/stdout', JSON.stringify(center(geojson)));
