/*
Given GeoJSON for a polygon on STDIN, print to STDOUT points inside the polygon and near
the border.

Visualize the results on https://geojson.io or your favorite mapping tools.

This can be used for testing the algorithm.

Ex: node test/find-expanded.mjs <test/data/bloomington-in-switchard-park.geojson
*/
import fs from 'fs';
import area from '@turf/area';
import buffer from '@turf/buffer';
import hexGrid from '@turf/hex-grid';
import center from '@turf/center';
import { getCoords, getType } from '@turf/invariant';
import boolPointInPolygon from '@turf/boolean-point-in-polygon';
import calcBbox from '@turf/bbox';

// Length of one of the six hex sides
const HEX_SIDE = 0.05;// in km
// "diameter" of a hex
const HEX_WIDTH = 2 * HEX_SIDE;
const HEX_AREA = 6495; // square meters

function getHexGrid(mask, bbox) {
  const options = {
    units: 'kilometers',
    mask,
  };
  return hexGrid(bbox, HEX_SIDE, options);
}

const featureCollection = JSON.parse(fs.readFileSync('/dev/stdin').toString());

const maybeExpandedFeatures = featureCollection.features.flatMap((feature) => {
  if ((getType(feature) === 'Polygon') && (area(feature) > HEX_AREA)) {
    // Find the bbox for the polygon.
    const bbox = calcBbox(feature);
    // Generate hexes that fit in the polygon
    const grid = getHexGrid(feature, bbox);
    // Return the centers of those hexes
    if (grid.features.length) {
      // Keep the point if it's within one hex width of the edge
      // To calculate that, buffer one hex width smaller and check if the points are outside that.
      const smallerPoly = buffer(feature, -HEX_WIDTH); // Turf.js defaults to km
      return grid.features
        .map(center)
        .filter((centerPoint) => {
          // The negative buffer process can cause an undefined result here, so reality-check
          if (smallerPoly && getType(smallerPoly) === 'Polygon') {
            return boolPointInPolygon(getCoords(centerPoint), feature)
              && !boolPointInPolygon(getCoords(centerPoint), smallerPoly);
          }
          // Small polygons that failed negative buffering don't need filtering.
          return true;
        });
    // If no hexes fit in the polygon, fallback to using the center.
    }
    return center(feature);
  }
  return feature;
});

fs.writeFileSync('/dev/stdout', JSON.stringify(maybeExpandedFeatures));
