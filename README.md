
## Walk Potential Calculator

This code is used to calculate "walk potential" across a city. It's based on
the [10 Minute Neighborhood](https://halifax.retales.ca/2018/08/10-minute-neighbourhoods/)
concept of having a neighborhood with having many common destinations within walking distance.

The code in its current state is specific to how it was used for Bloomington, Indiana.
It is being open sourced so that others may attempt it for their city or as a more general
tool.

## Usage

    WALK_POTENTIAL_ISOCHRONE_URL='https://example.com/otp/traveltime/isochrone?location={{lat}},{{lon}}&mode=WALK&cutoff=600S' \
    node ./scripts/calc-walk-potential.mjs <city-boundary.geojson >city-walk-potential-grid.geojson

## Defining walk potential

Locations with high walk potential have more categories of interesting destinations within
a comfortable walking distance.

Walk potential doesn't measure walkability because the comfort of the walk is not considered.
A neighborhood with no sidewalks across a wide, busy street from a strip mall might
have high walk potential but poor walkability.

Think of walk potential as measuring demand (places to walk to), while walkability measures
pedestrian supply (a comfortable walking environment).

Calculating walk potential is useful for urban planning. For example, to prioritize adding
sidewalks in locations that have high walk potential yet lack sidewalks.

## Ingredients

For this to work, you'll need:

 1. A boundary for a city, in GeoJSON format. Some city's run an "open data portal" where you might this.
    Some boundaries in the US are available through the [US Census TIGER/Line shapefiles](https://www.census.gov/cgi-bin/geo/shapefiles/index.php). The Coordinate Reference System (CRS) used must be EPSG:4326. This is the default for the GeoJSON standard but if results all come back empty or in the wrong location, this could be why!
    If your city is not available, you could use a county boundary.
 2. A web service that returns isochrones in GeoJSON format, such as OpenTripPlanner 2. Details below under WALK\_POTENTIAL\_ISOCHRONE\_URL.
 3. Node.js 16+, and an understanding how edit JavaScript files, install NPM packages and run Node.js scripts.

## Configuration

There are a few things you can customize by creating file named `.env` file in this project directory.

1. `WALK_POTENTIAL_ISOCHRONE_URL`: (required) A URL that returns isochrones in GeoJSON format, such as OpenTripPlanner 2. We will make a HTTP "GET" request to this URL to provide a latitude and longitude. It must return an isochrone for all 10-minute walks from there. Currently, authentication headers are not supported, but this is a feature that could be added. Example: 'https://example.com/otp/traveltime/isochrone?location={{lat}},{{lon}}&mode=WALK&cutoff=600S'
2. `WALK_POTENTIAL_ISOCHRONE_CONCURRENCY`: How many parallel queries we can make to the isochrone server to speed up the query. A good guess is the number of CPUs the server has. The default is 16. Lower numbers mean slower results, but a too-high value could overwhelm the server.
3. `WALK_POTENTIAL_AMENITIES_FILE`: Path to a JSON file that contains all the amenity categories to search for and Overpass Turbo queries used with them. [./amenities.json](./amenities.json) is used by default. See it for an example.
4. `WALK_POTENTIAL_BOUNDARY_URL`: URL which must return the area boundary in GeoJSON format in the EPSG:4326 CRS. The default is to read the boundary GeoJSON from STDIN as in the example above.
5. `WALK_POTENTIAL_GRID_FILE`: Path to a GeoJSON file containing a Feature Collection of Polygon objects, where each polygon represents a grid cell of the city for which walk potential will be calculated. The default is to generate a new grid based on `WALK_POTENTIAL_BOUNDARY_URL` or a provided GeoJSON city boundary.

All lines in the `./env` file are simple "key=value" pairs. Example:

```
WALK_POTENTIAL_ISOCHRONE_URL='https://example.com/otp/traveltime/isochrone?location={{lat}},{{lon}}&mode=WALK&cutoff=600S'
WALK_POTENTIAL_ISOCHRONE_CONCURRENCY=2
```

## How Walk Potential is calculated

 1. A hex grid (AKA hexbin) is generated that covers the entire city.
 2. For each of the categories of amenities defined below, [OpenStreetMap](https://www.openstreetmap.org) is queried to find
    all the locations of those amenities within the city boundary. The result of each of these
    is an "amenity layer".
 3. For each amenity layer, call OpenTripPlanner to generate the 10-minute walk isochrone
    for all amenities on the layer.  Combine all the isoschrones into a single feature. The
    boundary encloses all the locations that are within a 10-minute walk from that category
    of amenity.
 4. For each location in the hex grid start with a walk potential of zero, and
    check whether the center is within each amenity layer's
    isochrone. Add one to the walk potential for each amenity layer that is within a 10-minute
    walk.
 5. Values are scaled from 0-100 to make the walk potential value easier to compare
    to other values you might like to compare it with for each hex grid location.

## What's produced

The end result is an GeoJSON file _printed to STDOUT_ that contains a Feature
Collection of Polygons for the hex
features, where each hex has a `walkPotential` property.  Using a tool like ArcGIS Online
or QGIS, you can then visualize the relative walk potential of the city or include the walk
potental values in further calculations, such as calculating the priority of locations
throughout the city to prioritize filling in missing sidewalk gaps.


## Quality checks

The quality of the output is only is good as the quality of the location data
used in the calculations. Consider running each the amenity queries manually for your city
at [https://overpass-turbo.eu/](https://overpaass-turbo.eu). Are the results complete and accurate?

As a reference, try searching for the same category of amentity on another mapping service.

If some of these categories of amenities are missing from OpenStreetMap in your
area, consider editting OpenStreetMap to add them to get higher quality results. After updating
OpenStreetMap, you should be able to re-run the query on Overpass Turbo about 15 minutes
later to have your edits included in the updated results.

## Amenity categories

For the list of amenity categories used and the OpenTripPlanner queries used to
locate them on the map, see [amenities.json](./amenities.json).

The beauty of an open source project is that you can customize the details of
how walk potential is calculated for your city.

To support that, you can set `WALK_POTENTIAL_AMENITIES_FILE` in your `.env` to provide a path to an alternate file. The path should be absolute or relative to this project directory.

## Support

No support is provided. While inquiries, bug reports and pull requets are
welcome, expect to fix any bugs you find or add any features you are interested in yourself.

## Author

This project was created by Mark Stosberg <mark@stosberg.com>

On Mastodon at <a rel="me" href="https://urbanists.social/@markstos">@markstos@urbanists.social</a>

## Credits

Thanks to RideAmigos which provided resources to make the initial version possible. The following people
have provided feedback used to refine the amenity queries:

- [@SK53@en.osm.town](https://en.osm.town/@SK53)
- [Mark Kristensson](https://urbanists.social/@mkristensson)
- Mallory Rickbeil
